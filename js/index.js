
let svg = d3.select('#mysvg')

// 设置投影
let myProjection = d3.geoMercator().scale(1000).center([70.591796875, 55.494140625]).scale(1000).translate([0,0])
// 路径生成器
let path = d3.geoPath().projection(myProjection)

// 计算网格
let graticule = d3.geoGraticule()
graticule.extent([[69,16],[140,56]]).step([2,2])
let grid = graticule()

// 获取中国经纬度
let chinaJson = getChinaJson()
console.log(chinaJson.features)

// 绘制中国
svg.append("g")
    .selectAll("path")
    .data(chinaJson.features)
    .enter()
    .append("path")
    .attr("class", "province")
    .attr("d", path)
    .attr("fill", function(d){return 'rgb'+rgb()})

function rgb(){//rgb颜色随机
    var r = Math.floor(Math.random()*256)
    var g = Math.floor(Math.random()*256)
    var b = Math.floor(Math.random()*256)
    var rgb = '('+r+','+g+','+b+')'
    console.log(rgb)
    return rgb
}

// 绘制省名字
svg.append("g")
    .selectAll("text")
    .data(chinaJson.features)
    .enter()
    .append("text")
    .attr("class", "provinceName")
    .text(function(d){return d.properties.name})
    .attr("fill", "white")
    .attr('x', function(d){return lnglatToMercator(d.properties.cp)[0]})
    .attr('y', function(d){return lnglatToMercator(d.properties.cp)[1]})
    .style('font-size', '12px')

// 绘制网格
d3.select('body')
    .append("svg")
    .attr("class", "grid")
    .append("g")
    .append("path")
    .datum(grid)
    .attr("class", "graticule")
    .attr("stroke", "#ccc")
    .attr("d", path)

// 读取省地理区域json文件
function getChinaJson(){
    let chinaText = $.ajax({url:"/json/china.json",async:false})
    let chinaJson = decode(JSON.parse(chinaText.responseText))
    return chinaJson
}

// 读取省39节点json文件
function getGeoPositionJson(){
    let chinaText = $.ajax({url:"/json/geoPosition.json",async:false})
    let chinaJson = decode(JSON.parse(chinaText.responseText))
    return chinaJson
}

// 墨卡托转化
function lnglatToMercator(point){
    var p = []
    var point = [point[0],point[1]]
    var p =  myProjection(point)
    // console.log(p)
    p.push(point[0],point[1])
    return p
}
